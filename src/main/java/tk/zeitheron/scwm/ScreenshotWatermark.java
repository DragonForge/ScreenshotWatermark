package tk.zeitheron.scwm;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.cfg.file1132.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tk.zeitheron.scwm.config.ConfigsSW;

import javax.swing.*;

@Mod(modid = "scwm", dependencies = "required-after:hammercore", version = "@VERSION@", name = "Screenshot Watermark", updateJSON = "http://dccg.herokuapp.com/api/fmluc/388123", certificateFingerprint = "9f5e2a811a8332a842b34f6967b7db0ac4f24856", clientSideOnly = true, guiFactory = "tk.zeitheron.scwm.config.ConfigFactorySW")
public class ScreenshotWatermark
{
	public static final Logger LOG = LogManager.getLogger("ScreenshotWatermark");

	public static Configuration baseConfig;

	@Mod.EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with ScreenshotWatermark jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://www.curseforge.com/projects/388123 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put("scwm", "https://www.curseforge.com/projects/388123");
	}

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		baseConfig = new Configuration(e.getSuggestedConfigurationFile());
		ConfigsSW.reload(baseConfig);
		if(baseConfig.hasChanged()) baseConfig.save();

		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException err)
		{
			err.printStackTrace();
		}
	}
}