package tk.zeitheron.scwm.config;

import com.zeitheron.hammercore.cfg.file1132.Configuration;
import com.zeitheron.hammercore.cfg.file1132.io.ConfigEntryCategory;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;

import java.awt.*;
import java.util.Arrays;

public class ConfigsSW
{
	public static EnumRelative relative = EnumRelative.LEFT_UP;
	public static Point relativePoint = new Point(0, 0);
	public static int transparency;
	public static float scale = 1F;
	public static String imagePath = "";

	public static void reload(Configuration cfg)
	{
		ConfigEntryCategory root = cfg.getCategory("watermarking");

		ConfigEntryCategory rel = root.getCategory("relative");
		EnumRelative[] all = EnumRelative.VALUES;
		relative = all[rel.getIntEntry("anchor", 0, 0, all.length).setDescription("Relativity anchor.\n" + Joiner.NEW_LINE.join(Arrays.stream(all).map(e -> e.ordinal() + " - " + e.getName()))).getValue()];
		int rlx = rel.getIntEntry("x", 0, Integer.MIN_VALUE, Integer.MAX_VALUE).setDescription("X offset for relative position.").getValue();
		int rly = rel.getIntEntry("y", 0, Integer.MIN_VALUE, Integer.MAX_VALUE).setDescription("Y offset for relative position.").getValue();
		relativePoint = new Point(rlx, rly);
		imagePath = root.getStringEntry("Image", "").setDescription("Path to the watermark image. Leave empty to disable.").getValue();
		transparency = root.getIntEntry("Transparency", 127, 0, 255).setDescription("Watermark transparency when applying to screenshot.").getValue();
		scale = root.getFloatEntry("Scale", 1F, 0F, 10F).setDescription("Scale for when applying the watermark.").getValue();
	}

	public static void save(Configuration cfg)
	{
		ConfigEntryCategory root = cfg.getCategory("watermarking");

		ConfigEntryCategory rel = root.getCategory("relative");
		EnumRelative[] all = EnumRelative.VALUES;

		rel.getIntEntry("anchor", 0, 0, all.length).setDescription("Relativity anchor.\n" + Joiner.NEW_LINE.join(Arrays.stream(all).map(e -> e.ordinal() + " - " + e.getName()))).setValue(relative.ordinal());

		rel.getIntEntry("x", 0, Integer.MIN_VALUE, Integer.MAX_VALUE).setDescription("X offset for relative position.").setValue(relativePoint.x);
		rel.getIntEntry("y", 0, Integer.MIN_VALUE, Integer.MAX_VALUE).setDescription("Y offset for relative position.").setValue(relativePoint.y);
		root.getStringEntry("Image", "").setDescription("Path to the watermark image. Leave empty to disable.").setValue(imagePath);
		root.getIntEntry("Transparency", 127, 0, 255).setDescription("Watermark transparency when applying to screenshot.").setValue(transparency);
		root.getFloatEntry("Scale", 1F, 0F, 10F).setDescription("Scale for when applying the watermark.").setValue(scale);

		if(cfg.hasChanged()) cfg.save();
	}
}