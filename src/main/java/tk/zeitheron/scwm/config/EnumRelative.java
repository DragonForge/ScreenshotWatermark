package tk.zeitheron.scwm.config;

import net.minecraft.util.IStringSerializable;

import java.awt.*;

public enum EnumRelative
		implements IStringSerializable
{
	LEFT_UP(false, false, false, false),
	UP(false, false, true, false),
	RIGHT_UP(true, false, false, false),
	RIGHT(true, false, false, true),
	RIGHT_DOWN(true, true, false, false),
	DOWN(false, true, true, false),
	LEFT_DOWN(false, true, false, false),
	LEFT(false, false, false, true),
	CENTER(false, false, true, true);

	public final boolean xInvert, yInvert, xCenter, yCenter;

	EnumRelative(boolean xInvert, boolean yInvert, boolean xCenter, boolean yCenter)
	{
		this.xInvert = xInvert;
		this.yInvert = yInvert;
		this.xCenter = xCenter;
		this.yCenter = yCenter;
	}

	@Override
	public String getName()
	{
		return name().toLowerCase();
	}

	public Rectangle transform(Rectangle rect, int width, int height)
	{
		Rectangle pt = new Rectangle(rect);

		if(xInvert) pt.x = width - pt.x - pt.width;
		if(yInvert) pt.y = height - pt.y - pt.height;

		if(xCenter) pt.x = (width - pt.width) / 2;
		if(yCenter) pt.y = (height - pt.height) / 2;

		return pt;
	}

	public EnumRelative next()
	{
		return VALUES[(ordinal() + 1) % VALUES.length];
	}

	public static final EnumRelative[] VALUES = values();
}