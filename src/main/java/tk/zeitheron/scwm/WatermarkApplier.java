package tk.zeitheron.scwm;

import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ScreenShotHelper;
import net.minecraftforge.client.event.ScreenshotEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import tk.zeitheron.scwm.config.ConfigsSW;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@SideOnly(Side.CLIENT)
@Mod.EventBusSubscriber(Side.CLIENT)
public class WatermarkApplier
{
	public static BufferedImage createScreenshotNoWM()
	{
		Minecraft mc = Minecraft.getMinecraft();
		return ScreenShotHelper.createScreenshot(mc.displayWidth, mc.displayHeight, mc.getFramebuffer());
	}

	@SubscribeEvent
	public static void screenshotEvent(ScreenshotEvent e)
	{
		apply(e.getImage());
	}

	public static void apply(BufferedImage image)
	{
		Graphics2D g = image.createGraphics();
		{
			// perform watermark
			File file = new File(ConfigsSW.imagePath);
			if(file.isFile())
			{
				try
				{
					BufferedImage watermark = ImageIO.read(file);

					if(watermark != null)
					{
						float min = Math.min(image.getWidth(), image.getHeight()) / 4F;

						float ratio = watermark.getWidth() / (float) watermark.getHeight();

						Point offset = ConfigsSW.relativePoint;
						Rectangle rect = new Rectangle(offset.x, offset.y, (int) (min * ConfigsSW.scale), (int) (min / ratio * ConfigsSW.scale));
						rect = ConfigsSW.relative.transform(rect, image.getWidth(), image.getHeight());

						for(int x = 0; x < watermark.getWidth(); ++x)
							for(int y = 0; y < watermark.getHeight(); ++y)
							{
								int rgb = watermark.getRGB(x, y);
								rgb = ColorHelper.packARGB(ConfigsSW.transparency / 255F * ColorHelper.getAlpha(rgb), ColorHelper.getRed(rgb), ColorHelper.getGreen(rgb), ColorHelper.getBlue(rgb));
								watermark.setRGB(x, y, rgb);
							}

						g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
						g.drawImage(watermark, rect.x, rect.y, rect.width, rect.height, null);
					}
				} catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		g.dispose();
	}
}