package tk.zeitheron.scwm.gui;

import com.zeitheron.hammercore.client.utils.GLImageManager;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.math.MathHelper;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import tk.zeitheron.scwm.ScreenshotWatermark;
import tk.zeitheron.scwm.WatermarkApplier;
import tk.zeitheron.scwm.config.ConfigsSW;
import tk.zeitheron.scwm.config.EnumRelative;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GuiConfigureWatermark
		extends GuiScreen
{
	public static int glTexture1, glTexture2;

	GuiScreen parent;

	public GuiConfigureWatermark(GuiScreen parent)
	{
		this.parent = parent;

		ScreenshotWatermark.baseConfig.load();
		ConfigsSW.reload(ScreenshotWatermark.baseConfig);
	}

	@Override
	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);

		if(glTexture1 == 0)
		{
			glTexture1 = GL11.glGenTextures();

			image = WatermarkApplier.createScreenshotNoWM();
			for(int x = 0; x < image.getWidth(); ++x)
				for(int y = 0; y < image.getHeight(); ++y)
				{
					int rgb = image.getRGB(x, y);
					float b = 0.2126F * ColorHelper.getRed(rgb) + 0.7152F * ColorHelper.getGreen(rgb) + 0.0722F * ColorHelper.getBlue(rgb);
					b *= 0.25F;
					image.setRGB(x, y, ColorHelper.packARGB(1F, b, b, b));
				}
			GLImageManager.loadTexture(image, glTexture1, false);
		}

		if(glTexture2 == 0) glTexture2 = GL11.glGenTextures();

		getWatermark();
		if(wm != null) GLImageManager.loadTexture(wm, glTexture2, false);

		String text = "Change watermark image";
		int btnw = fontRenderer.getStringWidth(text) + 8;
		addButton(new GuiButton(0, (width - btnw) / 2, (int) ((height - 20) / 3.5F), btnw, 20, text));

		text = "Change relative anchor";
		btnw = fontRenderer.getStringWidth(text) + 8;
		addButton(new GuiButton(1, (width - btnw) / 2, (int) (height - 20 - (height - 20) / 3.5F), btnw, 20, text));
	}

	Thread jfct;

	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0 && (jfct == null || !jfct.isAlive()))
		{
			button.enabled = false;
			jfct = new Thread(() ->
			{
				JFileChooser jfc = new JFileChooser(mc.gameDir);
				jfc.setFileFilter(new FileFilter()
				{
					@Override
					public boolean accept(File f)
					{
						return f.isDirectory() || f.getName().endsWith(".png") || f.getName().endsWith(".jpg") || f.getName().endsWith(".jpeg");
					}

					@Override
					public String getDescription()
					{
						return ".png; .jpg; .jpeg";
					}
				});
				if(jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				{
					File sel = jfc.getSelectedFile();
					ConfigsSW.imagePath = sel.getAbsolutePath();
					loadWM = true;
					getWatermark();
					mc.addScheduledTask(() ->
					{
						if(wm != null) GLImageManager.loadTexture(wm, glTexture2, false);
					});
					ConfigsSW.save(ScreenshotWatermark.baseConfig);
				}
				button.enabled = true;
			});
			jfct.start();
		}

		if(button.id == 1)
		{
			ConfigsSW.relative = ConfigsSW.relative.next();
			ConfigsSW.save(ScreenshotWatermark.baseConfig);
		}
	}

	BufferedImage image;
	BufferedImage wm;
	boolean loadWM = true;

	public BufferedImage getWatermark()
	{
		if(loadWM)
		{
			loadWM = false;
			File f = new File(ConfigsSW.imagePath);
			if(f.isFile())
			{
				try
				{
					wm = ImageIO.read(f);
				} catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return wm;
	}

	@Override
	public void onGuiClosed()
	{
		Keyboard.enableRepeatEvents(false);

		// Deallocate texture to prevent GPU mem from clogging with useless to the game data

		GL11.glDeleteTextures(glTexture1);
		glTexture1 = 0;

		GL11.glDeleteTextures(glTexture2);
		glTexture2 = 0;

		ConfigsSW.save(ScreenshotWatermark.baseConfig);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();

		GlStateManager.color(1F, 1F, 1F, 1F);
		GlStateManager.enableAlpha();
		GlStateManager.enableBlend();

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, glTexture1);
		RenderUtil.drawFullTexturedModalRect(0, 0, width, height);

		UtilsFX.bindTexture("scwm", "textures/arrow_keys.png");
		RenderUtil.drawFullTexturedModalRect(width / 4.5F, (height - (31 + 1 + fontRenderer.FONT_HEIGHT)) / 2F + 1 + fontRenderer.FONT_HEIGHT, 46, 31);
		drawCenteredString(fontRenderer, "Offset watermark:", 23 + (int) (width / 4.5F), (int) ((height - (31 + 1 + fontRenderer.FONT_HEIGHT)) / 2F), 0xFFFFFF);

		UtilsFX.bindTexture("scwm", scaleRect.contains(mouseX, mouseY) ? "textures/scroll_bar_body_sel.png" : "textures/scroll_bar_body.png");
		RenderUtil.drawFullTexturedModalRect(scaleRect.x = (int) (width - width / 4.5F - 16), scaleRect.y = (int) (height / 2F - 24), scaleRect.width = 16, scaleRect.height = 48);

		UtilsFX.bindTexture("scwm", alphaRect.contains(mouseX, mouseY) ? "textures/scroll_bar_body_sel.png" : "textures/scroll_bar_body.png");
		RenderUtil.drawFullTexturedModalRect(alphaRect.x = (int) (width - width / 4.5F - 36), alphaRect.y = (int) (height / 2F - 24), alphaRect.width = 16, alphaRect.height = 48);

		if(!scaleAnchored)
		{
			String txt = "Scale: " + Math.round(ConfigsSW.scale * 10F) / 10F + "x";
			GlStateManager.pushMatrix();
			GlStateManager.translate(width - width / 4.5F - 8.5F, height / 2F, 0);
			GlStateManager.rotate(90, 0, 0, 1);
			GlStateManager.scale(0.75F, 0.75F, 0.75F);
			fontRenderer.drawString(txt, -fontRenderer.getStringWidth(txt) / 2, -fontRenderer.FONT_HEIGHT / 2, 0xFFFFFF);
			GlStateManager.popMatrix();
		}

		if(!alphaAnchored)
		{
			String txt = "Alpha: " + Math.round(ConfigsSW.transparency / 255F * 100F) + "%";
			GlStateManager.pushMatrix();
			GlStateManager.translate(width - width / 4.5F - 28.5F, height / 2F, 0);
			GlStateManager.rotate(90, 0, 0, 1);
			GlStateManager.scale(0.75F, 0.75F, 0.75F);
			fontRenderer.drawString(txt, -fontRenderer.getStringWidth(txt) / 2, -fontRenderer.FONT_HEIGHT / 2, 0xFFFFFF);
			GlStateManager.popMatrix();
		}

		UtilsFX.bindTexture("scwm", "textures/scroll_bar.png");
		RenderUtil.drawFullTexturedModalRect(width - width / 4.5F - 16, height / 2F - 24 + 44 * (1F - ConfigsSW.scale / 10F), 16, 4);
		RenderUtil.drawFullTexturedModalRect(width - width / 4.5F - 36, height / 2F - 24 + 44 * (1F - ConfigsSW.transparency / 255F), 16, 4);

		if(scaleAnchored)
		{
			String txt = "Scale: " + Math.round(ConfigsSW.scale * 10F) / 10F + "x";
			GlStateManager.pushMatrix();
			GlStateManager.translate(width - width / 4.5F - 8.5F, height / 2F, 0);
			GlStateManager.rotate(90, 0, 0, 1);
			GlStateManager.scale(0.75F, 0.75F, 0.75F);
			fontRenderer.drawString(txt, -fontRenderer.getStringWidth(txt) / 2, -fontRenderer.FONT_HEIGHT / 2, 0xFFFFFF);
			GlStateManager.popMatrix();
		}

		if(alphaAnchored)
		{
			String txt = "Alpha: " + Math.round(ConfigsSW.transparency / 255F * 100F) + "%";
			GlStateManager.pushMatrix();
			GlStateManager.translate(width - width / 4.5F - 28.5F, height / 2F, 0);
			GlStateManager.rotate(90, 0, 0, 1);
			GlStateManager.scale(0.75F, 0.75F, 0.75F);
			fontRenderer.drawString(txt, -fontRenderer.getStringWidth(txt) / 2, -fontRenderer.FONT_HEIGHT / 2, 0xFFFFFF);
			GlStateManager.popMatrix();
		}

		super.drawScreen(mouseX, mouseY, partialTicks);

		BufferedImage watermark = getWatermark();
		if(watermark != null)
		{
			float min = Math.min(image.getWidth(), image.getHeight()) / 4F;

			float ratio = watermark.getWidth() / (float) watermark.getHeight();

			Point offset = ConfigsSW.relativePoint;
			Rectangle rect = new Rectangle(offset.x, offset.y, (int) (min * ConfigsSW.scale), (int) (min / ratio * ConfigsSW.scale));
			rect = ConfigsSW.relative.transform(rect, image.getWidth(), image.getHeight());

			GlStateManager.enableBlend();
			GlStateManager.enableAlpha();
			GlStateManager.disableLighting();
			GlStateManager.color(1F, 1F, 1F, ConfigsSW.transparency / 255F);

			GL11.glBindTexture(GL11.GL_TEXTURE_2D, glTexture2);
			RenderUtil.drawFullTexturedModalRect(rect.x / (float) image.getWidth() * width, rect.y / (float) image.getHeight() * height, rect.width / (float) image.getWidth() * width, rect.height / (float) image.getHeight() * height);
		}
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(keyCode == 1) this.mc.displayGuiScreen(parent);

		EnumRelative rel = ConfigsSW.relative;

		if(keyCode == 19)
		{
			ConfigsSW.relativePoint.x = ConfigsSW.relativePoint.y = 0;
		}

		if((keyCode == 200 || keyCode == 17) && !rel.yCenter)
		{
			//up
			ConfigsSW.relativePoint.y += rel.yInvert ? 1 : -1;
		}

		if((keyCode == 208 | keyCode == 31) && !rel.yCenter)
		{
			//down
			ConfigsSW.relativePoint.y += rel.yInvert ? -1 : 1;
		}

		if((keyCode == 203 || keyCode == 30) && !rel.xCenter)
		{
			//left
			ConfigsSW.relativePoint.x += rel.xInvert ? 1 : -1;
		}

		if((keyCode == 205 || keyCode == 32) && !rel.xCenter)
		{
			//right
			ConfigsSW.relativePoint.x += rel.xInvert ? -1 : 1;
		}
	}

	boolean scaleAnchored, alphaAnchored;
	Rectangle scaleRect = new Rectangle(), alphaRect = new Rectangle();

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);

		if(scaleRect.contains(mouseX, mouseY) && mouseButton == 0)
		{
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
			scaleAnchored = true;

			float scale = 10F - MathHelper.clamp((mouseY - 1 - (scaleRect.y + 1)) / (float) (scaleRect.height - 2), 0, 1F) * 10F;
			scale = ((int) (scale * 10F)) / 10F;
			ConfigsSW.scale = scale;
		}

		if(alphaRect.contains(mouseX, mouseY) && mouseButton == 0)
		{
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1.0F));
			alphaAnchored = true;

			float alpha = 255F - MathHelper.clamp((mouseY - 1 - (alphaRect.y + 1)) / (float) (alphaRect.height - 2), 0, 1F) * 255F;
			ConfigsSW.transparency = Math.round(alpha);
		}
	}

	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
		if(scaleAnchored)
		{
			float scale = 10F - MathHelper.clamp((mouseY - 1 - (scaleRect.y + 1)) / (float) (scaleRect.height - 2), 0, 1F) * 10F;
			scale = ((int) (scale * 10F)) / 10F;
			ConfigsSW.scale = scale;
		}
		if(alphaAnchored)
		{
			float alpha = 255F - MathHelper.clamp((mouseY - 1 - (alphaRect.y + 1)) / (float) (alphaRect.height - 2), 0, 1F) * 255F;
			ConfigsSW.transparency = Math.round(alpha);
		}
	}

	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		if(scaleAnchored || alphaAnchored) scaleAnchored = alphaAnchored = false;
		super.mouseReleased(mouseX, mouseY, state);
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return true;
	}
}